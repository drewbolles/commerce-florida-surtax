<?php

/**
 * @file
 * Default rule configurations for Florida Tax Rate.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_tax_florida_default_rules_configuration() {

  $data = '{ "commerce_tax_rate_florida_tax" : {
    "LABEL" : "Calculate Florida Tax",
    "PLUGIN" : "rule",
    "REQUIRES" : [ "commerce_order", "commerce_tax" ],
    "USES VARIABLES" : { "commerce_line_item" : { "label" : "Line item", "type" : "commerce_line_item" } },
    "IF" : [
      { "commerce_order_compare_address" : {
          "commerce_order" : [ "commerce-line-item:order" ],
          "address_field" : "commerce_customer_billing|commerce_customer_address",
          "address_component" : "administrative_area",
          "value" : "FL"
        }
      }
    ],
    "DO" : [
      { "commerce_tax_rate_apply" : {
          "USING" : {
            "commerce_line_item" : [ "commerce-line-item" ],
            "tax_rate_name" : "florida_tax"
          },
          "PROVIDE" : { "applied_tax" : { "applied_tax" : "Applied tax" } }
        }
      }
    ]
  }
}';
  
  $rule = rules_import($data);
  $configs[$rule->name] = $rule;

  return $configs;
}
